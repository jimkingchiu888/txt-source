「唔～嗯，選哪個好呢？」

決鬥後的第二天的下午
艾麗婭食指抵著唇，正在煩惱著
場所是武器店薇兒卡斯

在短劍、匕首的區域的一角

昨天的決鬥
因為庫斯曼的猛攻，艾麗婭的小刀有兩個地方出現了大的卷刃
在判斷不可能修復後，決定重新做一把
之所以在下午來，是因為慣例的宿醉和店主經常在下午前一直待在迷宮裡

說到薇兒卡⋯⋯

「喵呀！塔瑪還是這麼溫順可愛呢喵呀！」

從艾麗婭那裡接過塔瑪，直接抱在從工作服裡露出的胸中，坦誠相對，看來是非常中意可愛的塔瑪，表情非常歡喜

「喵呀哦⋯⋯」

然後，塔瑪舒服地閉上了眼，意識朦朧起來
外表看似小孩，內心卻３０多歳大叔的騎士！
昨天的疲勞也未消退，已經睡眼惺忪了

「啊，這個好像很好的樣子」

在並列的許多小刀裡，艾麗婭終於找到了她中意的武器

（艾麗婭）直接把武器拿起來揮

咻──

揮舞的小刀響起了劃破空氣聲
比艾麗婭以前的武器還易於操縱，有確切的手感

「這個不錯呢。價格是⋯⋯唔⋯⋯」

露出了苦澀的表情
好用的武器價格也貴

昨天──在決鬥前的任務中，除了哥布林的耳朵，豬人的手也帶回了，兜裡還有點餘錢，現在要配一把和現在手上這把一樣的武器的話，幾乎要花光所有的儲蓄了
考慮到以後的武器維修費用，真是進退兩難啊

「喵呀、艾麗婭？雖然在煩惱於選擇武器的時候打擾你很不好意思，不稍微談談嗎？對艾麗婭來說也不是什麼壊事」
「怎麼了，薇兒卡要找我談話真是稀奇呢」

由艾麗婭發起的關於武器的談話倒是不少，薇兒卡和作為客人的艾麗婭談話倒是很少
有什麼事嗎？艾麗婭做出了不可思議的表情

「其實現在，在找隊友，可以的話不來組個隊嗎艾麗婭？」
「誒⋯⋯我和薇兒卡？」
「是的！雖然是我自誇，和Ｃ級的我組隊不賴吧」
「我很高興⋯⋯但是為什麼是我？我是Ｄ級而且薇兒卡已經有身為原騎士的櫻這麼好的隊友了⋯⋯」

因為薇兒卡的邀請而高興的艾麗婭

艾麗婭自己也想要女性隊友
至今一個人接受任務是因為，不擅長對應男性，周圍又沒有適合的女冒険者
在這種時候收到邀請，對方又是平日熟識的薇兒卡，而且等級比自己還高，當然很開心

但是，同時產生了一個疑問
就如艾麗婭所說，她是剛從新手畢業的Ｄ級
薇兒卡已經有了原騎士的Ｂ級冒険者櫻了

「等級的話不用擔心喵，我聽說艾麗婭是有潛力的冒険者，塔瑪的強大也在昨天的決鬥中有目共睹，被冒険者廣為傳頌了，還有⋯⋯」
「還有？」

薇兒卡點名表揚艾麗婭和塔瑪（大大的好）

但是，說到一半表情暗淡了下來閉口不談了，艾麗婭催促她繼續說下去

「還有就是櫻昨天解散了隊伍，據說是，懷上了從異世界穿越來的偽娘魔法使的孩子」
「這樣啊⋯⋯」

說是從異世界穿越來的也好，對突然出現的男生語氣的違和感也好，艾麗婭還是接受了
這個世界也經常有異界穿越的人，因為隊伍裡一個人懷孕了，隊伍就解散了的事情也經常有聽說
事實上，那個偽娘魔法使和櫻，好像和艾麗婭憧憬的「剣聖」有關係，這又是別的事情了

「所以不和我組隊嗎？現在的話，還附送以隊友優惠價提供武器的特典哦」
「你都這麼說了，我也不好拒絶，塔瑪，你也覺得不錯吧？」
「喵！」

答應了薇兒卡的重新勸誘的艾麗婭
被問的塔瑪也答應了

（對主人來說同伴增加是好事，喪命的風險也會變小，還有就是薇兒卡自己也開著店，非常值得信賴）

塔瑪也對和薇兒卡組隊沒有意見

「那麼，時間在明天早上，地點在公會門口怎麼樣？」
「好的，沒有問題，呼呼，請多關照，薇兒卡小姐」
「這裡才是請多關照」

艾麗婭和薇兒卡緊緊的握住了手

白色肌膚的美女精靈
小麥色肌膚的獸耳娘
還有元素貓一隻

在迷宮都市組成了不同膚色的隊伍