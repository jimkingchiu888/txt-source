﻿
──那一天，世界中的魔物和動物，在同時間裡東逃西竄。
從因為弱小而擁有高度警戒心和危險感知能力的魔物，到被國家認定為災害源頭的魔物。
所有一切，都好像在躲避著某種東西一樣地開始全力移動。　（所有的魔物都似乎像在躲避著某種東西一樣地開始全力逃竄，不論是那些因為弱小而擁有高度警覺的，或是那些因為強大而被國家認定為災害源頭的。）
本應該襲擊人類的魔物，卻沒顯示出要襲擊逃跑路途上的村子和城鎮的跡象。
只是，一心一意地繼續逃跑。
可是，沒有人知道那個理由。
──連它們是希望能夠從『巴帕德爾魔法學園』那裡，逃多遠就逃多遠的事也是。

◇◇◇◆◆◆◇◇◇

發生異變的，並不只是魔物們而已。
存在於『巴帕德爾魔法學園』裡的一切存在，都在同時間裡停下了動作。
他們全都是，全身流出大量的汗水，甚至連一根指頭也無法移動。
要說為何的話，他們都陷入了幻視當中。

『死亡』之手正搭在自己的肩膀上。
就如同『死神』一樣，並不只是掌管著死亡的存在。
但是，『死亡』這樣的非常存在，正把手搭在自己的肩膀上，在露出刻薄的笑容的同時，腦海裡便浮現出那冷淡的俯視的景象而已。
轉回頭看過去的話，就只有一死。
可是，誰也沒能明白在那其中的理由。
為什麼，會變成這種狀況呢......
這樣思考時，也完全沒有行動的工夫，只是一勁兒地害怕著突然出現在自己的背後那裡的『死亡』。
同時停下動作，變得萬籟俱寂的『巴帕德爾魔法學園』的內側──『柊誠一（怪物）』，富有餘裕地開始行動了。

◇◇◇◆◆◆◇◇◇

我，該如何做是好呢？
在這深不見底的心情裡......
該碰到哪兒是好呢？
由於接受了過去的自己和現在的自己，因此完全地適應了現在的身體，再加上固有技能『喜怒哀愁』的能力，確實體會到狀態值正以驚人的勢頭持續地上升。
啊啊......
就是這麼簡單呢。
向我最心愛的人們出手──要是能碰上凱澤魯帝國的話就好了。
能用迄今為止最殘酷的方法，將之殲滅吧。
就算這麼說，但很遺憾，我不知道該怎麼做才能讓對方感到絕望。
如果這件事讓公會本部的愛麗絲來做的話，說不定『SM講座上級篇』能大受歡迎也實在太好了。
算了！
無論是哪邊......
都太遲了。
沒有辦法，也毫無根據
──不好意思讓你連存在都消失了，原諒我吧。
我，創造了全新的魔法。
殘酷，沒有慈悲地，毫不留情地連（在別人心中的）記憶都完全消滅掉的魔法。
沒有任何東西會被留在腦海裡。
要說為什麼的話，那就是將一切都給消滅掉。
在我正要站起來的時候，不知為何突然站住的神無月前輩靠近過來，我一邊強烈地想像著在未知土地上的凱澤魯帝國，一邊打算說出魔法的名字。

「Ze──。」
『誠一！』

在那個瞬間，不知何人，緊緊地抱住了我的身體。
於是便立刻冷靜地思考下來，並把目光轉向緊緊抱住我的人們那裡──

「誠一！沒事嗎！？」
「冷靜下來，誠一！」
「主人！請冷靜下來！雖然不明白理由，但吃點美味可口的食物吧！那樣的話心情也應該會平靜下來！」
「......誠一歐尼醬。就那以上，不行！」

阿爾和露露妮緊抱著我的手腕不放，莎莉婭則摟住腰部那裡，而奧爾嘉緊緊地抱住大腿不放。
看到了停下我的動作的人物後，存在於我心中的那深不見底的心情，感覺到一下子就平息了下來。

「大，大家......為什麼......？」

發愣和嘟噥了那句話後，阿爾回答道。

「雖然不是很明白......但不在這裡阻止誠一的話，我想以後誠一絕對會後悔的哦......。」
「後悔......。」

聽到了阿爾所說的話後，我才發現自己即將做出毫無道理的事。
我，甚至打算從世界上的人們的記憶裡抹掉有關凱澤魯帝國那個東西。
抹消的對象，不只是凱澤魯帝國的偉人們而已，還包含國民在內。
注意到這種事情的瞬間，我完全清醒過來了。

「誠一的力量，在生氣時使用是沒有意義的喲？如果是平時那樣的誠一使用的話，既有趣又好笑，能讓周圍的人們幸福起來哦！」

站在我的正前面的莎莉婭，笑臉地說出那句話。
隨著莎莉婭所說出的話語後，露露妮也點頭了。

「就是那樣哦，主人！向左道旁門等等的人展示出主人的力量也實在太可惜了。不，把憤怒轉向他們也太愚蠢可笑了。不久後，想到要向主人屈服的世界也能隨意地排除掉哦。說起來主人......生氣的話肚子裡也會空空如也吧？一起地再一次去吃飯吧！」
「不是要稍微地控制於三餐內嗎！？這樣下去我的待遇可不妙哦！」

對於露露妮所說的話，我忍不住地吐槽。
話說回來，為什麼露露妮會知道我想要征服全世界啊！？
無論是誰也沒有說過哦！？
於是，莎莉婭她們全員露出了笑臉。

「終於回到了平常的誠一呢！」
「真是的......雖然不知道理由，但剛才對誠一的氣場變化感到很驚訝呢......。」
「我是知道理由的哦......。」
「知道的哦！？」
「......我也，知道......。」
「奧爾嘉也知道！？」

阿爾揚起了驚愕的聲音。
於是，露露妮和奧爾嘉都指著神無月前輩的手鐲。

「恐怕，原因就是那個了！」
「......嗯，阿爾......很壞！」
「啊？......什麼啊？好像在哪裡見過那個設計呢......。」
「......『隸屬的手鐲』，是我被裝上的『隸屬的項圈』的劣化版......我也，在剛才，注意到了那個......。」
「什麼！？是，是那樣的啊！？」
「......啊啊......。」

雖說會再次變成暗黑的心情，可是這次不會成為任憑怒火上身的狀況了。
我，富有餘裕地靠近神無月前輩。

「神無月前輩......。」
「哈！？我，我到底......？」

不知為何，神無月前輩流出了許多的汗液，因此我感到了疑問。
突然間，我注意到周圍都異常安靜，便環視四周，為什麼全員都倒在地板上，又或者趴在桌子上呢，並發展成不可思議的景象。

「哈！？這個怪異現象是什麼啊！？」
「不，這是誠一的原因哦！」

阿爾，藐視地盯著我這樣說道，並對於我沒自身的記憶，只能感到納悶。
於是，莎莉婭便說了出來。

「因對誠一那憤怒的心情吃不消，所以從無法行動中被解放出來的時候，在感到安心的同時便昏了過去。」
「誒？是因為我的憤怒嗎！？」

這個步行兵器是什麼啊！
隨隨便便就能將感情給爆發出來呢！
說不定，真的地哭泣起來的話就會形成了一個湖泊哦？
不，難道說
──果然還是得否定這個呢！

「可，可是，確實是擁有著『威壓』的技能，但那個技能應該是有著不少的差別和不同的意義吧......。」
「以誠一的情況來看，那並不是因為技能之類的效果而引起的，純粹只是誠一那自身的力量，使得大家的生存本能大大地敲響警鐘。」

我好像即使沒有使用技能也能夠輕易地壓制別人。
嘛，如果看到我那狀態值的話，也會理解的呢！
並沒有習以為常的跡象呢！

「神無月前輩！沒事嗎？」
「啊，是的，誠一君也沒事嗎？我，在一瞬間內陷入了屈服我所有一切的感覺......不知為什麼，會感到非常愉快呢。」
「這才不是沒事呢！」

雖然不是很明白，但我的憤怒導致了所有人都陷入昏迷當中，也認為對這感到心情良好的神無月前輩並不正常。
在哪裡有醫生啊？
這裡有重症者哦！
雖然有點頭暈，但對著站在原地的神無月前輩，我用嚴肅的表情開口說了出來。

「神無月前輩，那個手鐲......是怎麼回事？」
「這個嗎？這個，是從凱澤魯帝國那裡收到的。不管怎麼樣，都好像能補正我們的狀態值。」
「......。」

果然是凱澤魯帝國沒錯呢......
而且，從神無月前輩的口氣來觀察的話，勇者們全員都裝備著那個東西吧。
儘管如此，受到莎莉婭她們的阻止的我，已經不會再次將憤怒給爆發出來了。
在這裡說不定可以使用魔法的『審判』，但國家的領導者突然消失變得不在的話，凱澤魯帝國的國民也會感到困難的吧，那是因為不知道他們在實行著些怎樣的政治。
但是，也不是說我消了氣。
所以，我會找個法子，以個人的名義給他點臉色看看。
這不是預定的，而是決定好的事項。

「神無月前輩......請好好地聽著！前輩們所帶著的手鐲，是被稱為『隸屬的手鐲』，那是為了讓裝備那個的人強制地遵從安裝者的道具。」
「什麼！？可是，我們所擁有的技能『鑒定』，並沒有顯示出那樣的東西......。」
「我有著各種各樣的技能，其中還擁有著比那個『鑒定』來得更加高級的『高級鑒定』。除此以外，也持有『世界眼』這樣的固有技能，所以才能夠確認對象的狀態值......神無月前輩的狀態是，『隸屬』」
「什！？」

對於我的說明，神無月前輩大吃了一驚。

「為什麼誠一君會擁有著如此優秀的技能呢？」
「......說來話長，有時間的時候再說吧。比起這樣的事情，首先是要先解決和取下神無月前輩所裝備著的『隸屬的手鐲』。雖然理由是不明的，但那一切都超出了前輩們的『鑒定』，並被騙了。」
「怎麼會......。」

神無月前輩好像受到了極大的打擊。
這也是這樣的吧。
當認為某樣東西對自己有利的時候，往往也就只會有更多的過失而已。

「幸虧，我有著能從那個狀態裡解放的術式。因此，現在馬上就能解放──。」
「稍等一下，誠一君！」
「誒？」
「要是你所說的一切都是真的話，召喚我們的國家......凱澤魯帝國的一側是有意圖，才會安裝那個在我們的身上。如果以解除了那個的狀態再次回到那個國家，那不就是明擺著會變成麻煩的事情嗎？」
「這樣的話，不回去那邊不就行了啊！」
「說成那樣也不行的！」
「為什麼！？」
「......不想給你添加更多的擔心了......我們在被召喚的時候，老師他們就作為人質被捉住了。」
「什！？」
「不只是老師他們，恐怕沒有作為勇者的適應性的人們......也就是說，不擅長戰鬥的學生們，也好像一樣被監禁起來了。」
「......。」

我，也只能默不作聲而已。
那個消息，對我來說是富有衝擊性的內容。

「我不能對陷入那種情況的他們棄而不顧，因為我好歹也是個學生會長。」
「......。」

確實，就算是我也不會想著要拋棄他們。
但是，對我來說，應該最優先的對象，是神無月前輩她們。
我並非是聖人君子。
雖然這樣做會被其他人說些什麼，但對我來說，神無月前輩她們以外的也只不過是無名角色而已。
而且，即使我自己打算使用魔法來移動到凱澤魯帝國，但是這是辦不到的。
要說為什麼的話，『空間魔法』的轉移，一次也沒有去過的地方是不能發動這個魔法，即使利用我的『魔法創造』，也無法顛覆這個限制。
要說為什麼的話，也就只是完全無法想像出來而已。
即使用『魔法創造』來創造可以看見遠方的景色的魔法，但條件並不是『看過的地方』，而是『去過的地方』，因此恐怕是不行的吧。
在這以前，正因為存在著『轉移』這樣的魔法本身，所以才沒發動『魔法創造』。

「這樣的話，該怎麼做呢──。」
「就是那個！誠一君，在不弄壞我的手鐲下把那個給取下。」
「誒？」

不，不弄壞嗎？
不對，我的『林O大總統』曾經把奧爾嘉的項圈給弄壞了，所以那樣的事情能夠辦得到嗎？
......只要做到沒有弄壞的影像，就能通過那個影像的感覺來發動了。
雖然不是很明白那個理由，但我是這麼想著的。
如何說呢，由於完全熟悉了我的身體和意識，所以能夠感覺到已經傳達到我的體內了。
......就算不行也只能試著做嗎？
請求了神無月前輩拿開手鐲的遮蔽物後，我抱著普通地脫落的印像，詠唱了『林O大總統』。
於是──

「真的解開了！」
「真的做到了！」
「不，你是不知道怎麼做的嗎！？」

漂亮的，手鐲在沒損壞的情況下，被取下了。
神無月前輩拾起那個脫落的手鐲，並向我伸出了那個。

「好的，再次幫我裝上！」
「去醫院吧！」

為什麼會說道要再次裝備已經取下的東西呢！？
那很奇怪哦！？
對於神無月前輩那意義不明的行為，我反射性地說出了那些話，但神無月前輩沒有顯示出特別感到在意的樣子，並以認真的表情一口斷定。

「我想讓你支配！」
「太遲了......！」

雙手抱著頭，我大聲地喊道，而神無月前輩則以認真的表情繼續說道。

「有百分之零點一是開玩笑的。」
「也就是說百分之九十九點九是認真的咯！？」
「嘛，冷靜下來！就如剛才所說的，以沒有戴著手鐲的狀態回到帝國的話，就會變成麻煩的事情吧。有關這點，是沒錯的。」
「是這樣啊。」
「那樣的話，只要更改安裝者就行了。這樣做的話，不但不會敗露給對方，而且還能讓你支配......說不定我是天才呢！」
「我討厭這種天才啊！」
「無論如何，這並不是命令，所以再一次地為我裝上，拜託了！」

以異常認真的表情這樣說道的神無月前輩。
嘛，所說出來的事雖然都能夠理解，但......
繼續糾葛的時候，神無月前輩突然用我的手臂，就這樣幫她裝上了那個手鐲。

「啊！？」
「呋呋～～這樣一來，我就是你的東西了！」

不知為何，神無月前輩以出神的表情說出那樣的話語。

「那麼，趕快命令我吧！」
「剛才才說過不是命令的嗎！？絕對說過的哦！」
「那是騙人的！我在這種夢寐以求的狀況下才不會成為老實人呢。」
「好了，趕緊命令我吧！」
「立場逆轉了！？」

雖然本應該是由我持有著下達命令的權力，但不知為何神無月前輩卻向我下達命令。
而且那個內容還偏偏是『你來下令』......討厭這個人。
盯～～繼續地盯著我，意外的感到非常不舒服。
在想著她到底是怎麼了的時候，我突然間想到了。

「那樣的話，神無月前輩。」
「好的，是要和我一起共度良宵了嗎？就交給你了！」
「前輩是在用著怎樣的目光來看待我的啊！？」

咳了一聲後，我再次轉過身向著神無月前輩。

「那麼就下達命令吧！『絕對要平安無事』......這個就是對前輩所下達的命令。」
「誠一君......。」
「對我來說，最優先的就是神無月前輩們。雖然是非常殘酷的說法，但只要神無月前輩們平安無事的話，其他學生變成變成怎麼樣也無所謂，比起什麼都好，前輩們是最重要的。」
「......。」
「前輩好像在逃避著一切一樣地說出從今以後不要將我給捲進來，但是要解除翔太他們的手鐲的話，絕對還是會去見面的。」
「......全員的手鐲......完全說不出這話來。因為你並沒有那個義務，我比起什麼都好都不想把你給捲進來......。」
「神無月前輩......。」
「......我明白了！這樣的話，就取下翔太他們的手鐲吧。在那以後，就和翔太他們一起合作，無論如何都要保護好大家。」

說出那樣的話時，神無月前輩用笑臉對我說道。

「當然，在守護你的命令的同時......要是有更加殘酷的命令的話就太好了呢。」

最後那句話，聽到了哦。
