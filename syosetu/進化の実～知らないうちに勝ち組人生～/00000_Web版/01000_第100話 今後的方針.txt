﻿
我───蕾雅‧法爾莎，為了把在魔王軍議會決定的與溫布爾格王國交流的事情傳達給部下的貝爾他們，回到了我的城堡。
一到達城堡後，便直接往貝爾他們的房間移動。
接著，毫不猶豫的打開了房間的門。

「右、右腳嗎......！是、是紅色的吧！？」
「等......貝爾先生！把手放在那邊的話，我的手不就放不到綠色的了嗎！」
「吵死啦！不管怎樣給我辦到！喂，伯斯格！你平常就那樣了，因為要用身體碰到區塊，所以稍微給我注意一下啊！變瘦，現在馬上給我變瘦！」
「請、請不要說些不可能的事啊！」
「好～，繼續下一個囉～。......好！接下來是左腳藍色！」
「左腳藍色！？」
「等......碰不到......！」
「好，下一個～。......話說啊，玩這遊戲的只有男的嗎？光是看都讓人想吐啊......。」
「「這種事情我們是最懂的啊！」」

我把門關上了。
......什麼啊？那個
不是很明白，在鮮豔的地毯上，貝爾和提利兩個人用奇怪的姿勢纏在一起的樣子。
然後，用冰冷的目光看著的伯斯格，手邊的那個也很鮮豔呢......在用著確實叫做輪盤的這種東西，不知道為什麼對著貝爾他們發出指示......。
該不會，在我不在的期間，貝爾和提利變成那種關係了？明明我都還沒有男朋友！
話說，那些傢伙不是有妻子的嗎！？沒問題嗎！？
......不對，要決定還太早了。或許只是我誤會了也說不定......。
我再次打開門確認裡面的樣子。

「咈喔喔喔喔喔！」
「太近了太近了太近了太近了！」
「嗚哇......好噁......。」

擺出拱橋的姿勢的提利上方，貝爾那傢伙以像要覆在他身上的模樣映入我的眼裡。
我把門關上了。
......。
似乎不是誤會......。
不管怎麼看都是那種關係吧，那個......。
什麼啊？在我不知道的時候發生了什麼？只是把他們丟在一邊，就發生什麼錯了嗎？咦，是我的錯？
......不對，是那些傢伙不好喔。明明都有老婆了......。
好，去跟他們老婆告密吧。
這麼決定的我，用力把門打開了。

「你們，是那種關係呢......。」
「誒！？咦蕾雅大人啊啊啊啊啊啊啊啊啊！？」
「啊，貝爾先生！等......放鬆的話......！」
「啊。」

伯斯格發出那種呆愣聲音的瞬間，貝爾的體勢崩壞，從提利的上方壓了下去。
那時，我清楚的看見了。
沒錯───。

「嗚噎噎噎噎噎噎噎噎噎！」
「嗚噁噁噁噁噁噁噁噁噁！」
「好噁......！」

提利和貝爾Kiss了。
我以到達無的境界，向貝爾他們說了。

「你們......是那種關係呢......對不起，沒有察覺到......。」
「誤會！蕾雅大人，這是盛大的誤會啊！」
「不，那個場面可沒辦法說是誤會吧～」
「伯斯格格格格格格！你也給我幫忙解開誤會啊！」
「啊啊......跟貝爾先生......抱歉啊......我的嘴唇，沒辦法守住你......請原諒我這不中用的丈夫......。」
「連提利也！？啊啊啊！為什麼會變成這樣！？」

────在那之後，從貝爾那聽完話的結果，對貝爾他們的誤會解開了。
據說似乎是異世界的遊戲的樣子。
原來如此呢。

「───所以說，是在玩呢？」
「啊，踩到地雷了。」
「「貝爾先生啊啊啊啊啊啊啊！」」

我做為處罰，用拷問的全餐招待了貝爾他們。
全餐結束後，在我眼前有三個破破爛爛，離死亡只差一步的垃圾倒著。

「死、死了......要死了......。」
「已經不要了......鐵棒、不要......。」
「啊啊......鐵球要......鐵球要......。」

雖然很像在說著什麼夢話，不過我無視那個直接進入本題。

「你們，給我打起精神。」
「誒？」
「在魔王軍議會，決定之後我們會與溫布爾格王國進行交流了。」
「咦！？」

貝爾他們果然對我的話感到震驚了。

「還真突然呢......為什麼又？」
「露媞亞大人決定的呦。接著做為那第一步，選擇了溫布爾格王國。那裡是從以前開始就對魔族也表示友好的少數國家。然後，因為你們也會做為護衛之一參加會議，給我把這件事記在腦袋裡。」
「瞭解了......啊。」

貝爾用理解的樣子點了點頭，馬上在途中變成想到了什麼似的表情，臉色立刻變青了。

「蕾、蕾蕾蕾雅大人？」
「什麼啊？」
「那個......溫布爾格王國，很像被灑過轉移魔法......又很像沒被灑過轉移魔法......。」
「......。」

話說回來是這樣呢。
然後，我因為那件事正在考慮著處罰呢。
我無言的取出鞭子後，就這樣把清理完的拷問器具拿出來。

「啊、啊勒？蕾雅大人？不是剛剛才處罰完───。」
「......。」
「不說話！？不說話才最恐怖啊！？」
「貝、貝爾先生！」
「不要慌張！我們還有能用───。」
「那麼開始了喔。覺悟吧？」
「騙人吧啊啊啊啊啊啊！」

◆◇◆

「───就這樣，回來了。」
「就這樣！？」

安全的從冥界歸來，完成與莎莉亞她們的再會的我......柊誠一，跟著莎莉亞前往F班。
然後，阿格諾斯他們就在教室裡，就這樣進行歸來的報告了。
還以為發生了那樣的事後，肯定會想要回家的，不過阿格諾斯他們以自己的意願留下來了的樣子。
之後，聽了我的說明的阿格諾斯他們，跟預料中的一樣十分震驚。

「也就是說，在冥界相遇後一起回來的是我的雙親和，勇者一行人、勇者的師傅跟初代魔王。然後是花店小姐。」
「你們好，我們是誠一的雙親。」
「我是勇者。」
「我們是那個勇者的同伴。」
「我是師傅。」
「魔王～喔。」
「我、我是花店小姐？」
「很好，自我介紹也做完了。」
「這樣就沒了！？」

本來就打算做個大致的自我介紹就結束的，不然就太過複雜了。嗯，我也這麼想。

「你啊，也太過亂七八糟了吧！？本來真的已經死了，還從冥界那把其他人帶回來什麼的，常識偏差過頭了吧！？而且是勇者和魔王......我們的腦袋差不多要爆了啊！？」
「哎呀......還真無法反駁啊！」
「這不是能笑的事吧！？」

海蓮的話讓我突然笑了起來。
真的，我到底做了什麼啊！
在我自暴自棄的笑著時，貝雅托莉絲小姐用不太相信的表情向我發問了。

「真的是......真的是誠一先生嗎......？」
「嗯嗯，我回來了！」
「真的......？」
「是的！」
「真的是真的......？」
「是、是的。」
「真的是真的是真的───。」
「相信我啊！？」

不，如果立場相反我也不會信就是！
貝雅托莉絲小姐總算相信我的話的樣子，眼眶浮現淚水，微笑著。

「真的是......真的是太好了......！」
「......讓妳擔心了。」

事實上，確實讓莎莉亞她們擔心了。
那不反省可不行，得要慎重的行動啊。......這個，雖然我覺得同樣的事情遇過好幾次了，不過這不是那麼簡單就能做到的事啊。要適可而止，雖說我想成長是很好。只有身體進化過頭，內心不跟著成長可不行呢。
總上所述，進化的步調，不會落下？

「我可是相信的喔！是大哥的話就能辦到的！」
「嘛，我也是，不會懷疑關於誠一老師的實力呢。......雖然比我想得還要非常識。」
「那股信賴讓我現在很難受。」

被做為非常識來信賴在各方面都讓人感到難受啊！
不過，阿格諾斯和布魯德也沒有改變讓我安心了，雖然還沒經過一天。
接著，我望向貝亞德和雷歐。

「感覺怎麼樣？」
「是、是是是是的！感覺很好！」

雷歐用非常緊張的模樣這麼回答。

「喂，真的沒問題嗎？」
「沒、沒問題！......那個，我也......雖然只有一次，為了大家而戰鬥了......稍微，有點自信了。」
「是嗎。那麼，之後就能和阿格諾斯他們一起戰鬥了呢。」
「耶耶耶耶耶！？做做做做、做不到的！像我這種人和阿格諾斯同學們一起什麼的......！太過不勝惶恐會死的喔！啊，我居然頂撞了老師！？對不起對不起對不起。」
「果然沒變啊！」

還以為多少有點積極了，結果還是變得跟原本一樣呢。
忍不住苦笑時，貝亞德用認真的表情向我搭話了。

「誠一老師。」

和之前不同，把熊的布偶裝脫下也不用寫生版，而是發出自己的聲音了。
貝亞德無法發出聲音的原因是，臉因為大火而受傷。

「貝亞德。怎麼了？啊，之前突然就治好了，剛好又沒有問，能治好太好了呢？」
「啊啊。」

簡短的回答後，貝亞德就這樣深深的低下了頭。

「誠一老師，謝謝你。我啊，因為那個燒傷的關係而被大家所害怕著。所以希望多少能夠不被恐懼才穿上了熊的布偶裝。而且，敵方的那個女性......她是治理我的故鄉的領主的女兒。並不是特別有關係，即使如此她也多虧了誠一老師，多少能夠取回一些東西的話......我除了向你表示感謝外也做不到什麼。不好意思。不過......真的很謝謝你。」

貝亞德用著有些笨拙但很確實的聲音，這麼說了。

「不會，不用在意......但也不能說得這麼簡單呢。你的感謝我確實收下了。」
「......謝謝你。」

像這樣，打完一圈招呼後，我向貝雅托莉絲小姐問了一件事。

「之後這個學園會怎麼樣呢？」
「現在，學園長在拚命對應著，會怎麼還不知道。不過說了一定會再開的喔。」
「是這樣嗎......那麼，定期試驗也沒有了嗎？」
「嗯嗯。總之，先保留呢。」
「太棒啦啊啊啊啊啊啊！我的大勝利啊啊啊啊啊啊！」

聽到貝雅托莉絲小姐的話，阿格諾斯吼著。
不管是哪裡的學校，都有像這樣考試沒了的瞬間就會情緒爆發的學生呢。我也是學生的話也會超高興就是。

「喂，笨蛋。吵死了。」
「哈！平常的話會好好收拾你，不過現在我心情好的很！所以我就當耳邊風吧！」

在阿格諾斯心情很好的說著那種事情時，貝雅托莉絲小姐丟下了無情的炸彈。

「不過呢，要像總有一天會遇到考試那樣，繼續考試的學習。」
「───喂布魯德喔喔喔喔喔喔！把我當笨蛋是怎樣啊啊啊啊啊啊！」
「......不是說當耳邊風嗎......。」

布魯德呆愣的嘟嚷著。
......嘛，在其他學生回到自家時，阿格諾斯他們則是以自己的意思留下來的樣子呢。在那段時間，因為學校也沒了，就用來做考試學習也不錯吧。就算沒有考試，也是有為了學習而學習的事情呢。
就這樣，我們F班就像總有一天會遇到考試那樣，開始準備了。

